package com.example.assingnment_weekend_santoshbhatt

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.hide()

        val frag = FragmentFirst()
        val transaction =supportFragmentManager.beginTransaction()
        transaction.add(R.id.fragment,frag)
        transaction.commit()

    }
}